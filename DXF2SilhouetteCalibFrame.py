#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) 2020 Ellen Wasboe, ellen@wasbo.net
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""
Add rectangle matching the resize size in Silhouette Studio 
for use when exporting to DXF14 using the ROBO option
"""

import inkex
from inkex import PathElement

class DXF2SilhouetteCalibFrame(inkex.EffectExtension):

	def add_arguments(self, pars):
		pars.add_argument("--tab")
		pars.add_argument('--colorString', default='blue')
		pars.add_argument('--opacity', default='0.1')
		pars.add_argument('--size', default='12_12')
		
	"""Add rectangle"""
	def effect(self):
		
		if self.options.size == '8_12':
			width_mm=170.02
			height_mm=237.65
		else:
			width_mm=258.92
			height_mm=263.05
			
		w = self.svg.unittouu(str(width_mm) + 'mm')
		h = self.svg.unittouu(str(height_mm) + 'mm')
				
		d = ' '.join(str(x) for x in
			['M', 0, 0,
			'L', 0, h,
			'L', w, h,
			'L', w, 0,
			'Z'])

		calibBox = PathElement()
		style = inkex.Style({'stroke': 'none','stroke-width': 0,'fill-opacity':self.options.opacity})
		col=inkex.Color(color=self.options.colorString)
		style.set_color(col, 'fill')
		style.set_color(col, 'stroke')
		calibBox.style = style
		calibBox.label = "CalibrationBox"
		calibBox.path = d
		self.svg.get_current_layer().append(calibBox)
		
if __name__ == '__main__':
    DXF2SilhouetteCalibFrame().run()

